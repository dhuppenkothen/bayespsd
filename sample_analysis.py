import glob
import matplotlib.pyplot as plt
from pylab import *
import matplotlib.cm as cm
### Some code fragments to run the analysis on the big sample
import lightcurve
import powerspectrum
import xcor
import generaltools as gt
import mle

import scipy.optimize

### run analysis on all bursts

import runanalysis

def run_allbursts(filename, namestr="test", stack=False, stackbounds = []):
## example
    alldict, burstlist = runanalysis.runanalysis(filename, stack=stack, namestr=namestr, stackbounds=staackbounds)

### loop over all bursts and do Bayesian analysis:

    for key, list in alldict.iteritems():
        counter=0     
        for l in list:
            print("I am on file " + str(key) + ", burst " + str(counter))
            l.bayesian_analysis(namestr=key + "_b" + str(counter), nchain=500, niter=200, nsim=1000)
            l.save_burst(key + "_b" + str(counter) + "_burstfile.dat")
            counter= counter+1

    return alldict

### make light curves

def sample_lcs(alldict):
    for key, list in alldict.iteritems():
        counter = 0
        for l in list:
            counter = counter + 1
            lc = lightcurve.Lightcurve(l.time, timestep=0.0001)
            plot(lc.time, lc.counts, lw=2, color='navy', linestyle="steps-mid")
            plt.axis([min(lc.time), max(lc.time), min(lc.counts)/2.0, max(lc.counts)*2.0])
            plt.title("Obs " + str(key) + ", burst " + str(counter) + ", blen = " + str(l.blen) + ", fluence = " + str(l.fluence))
            plt.savefig(str(key) + "_b" + str(counter) + "_lc.png", format="png")
            plt.close()
    return




def plot_stacked(stack, fnyquist = 4096.0, colormap=cm.prism, mult=50, png=True, namestr="test", legend=None, rescaled=False):


    freqlist = []
    for i,s in enumerate(reversed(stack)):
        freq = np.arange(len(s)-1)*fnyquist/float(len(s)-1) + fnyquist/float(len(s)-1)
        freqlist.append(freq) 
        if rescaled:
            loglog(freq, s[1:]*10**(i/2.0), lw=2, linestyle="steps-mid", color=colormap(i*50))
        else:
            loglog(freq, s[1:], lw=2, linestyle="steps-mid", color=colormap(i*50))

    plt.xlabel("Frequency [Hz]", fontsize=16)
    plt.ylabel("Averaged Leahy Power", fontsize=16)
    if not legend==None:
        plt.legend(legend)

    if png:
        plt.savefig(test + "_stacked.png", format="png")
    return




def readout_variability():
    filename = glob.glob("*findperiodicity_results.dat")
 
    theta0, theta1, theta2, theta3, theta4 = [], [], [], [], []
 
    for file in filename:
        f = open(file, 'r')
        for i, line in enumerate(f):
            if "theta[0]" in line:
                theta0.append(line.split()[1])
            if "theta[1]" in line:
                theta1.append(line.split()[1])
            if "theta[2]" in line:
                theta2.append(line.split()[1])
            if "theta[3]" in line:
                theta3.append(line.split()[1])
            if "theta[4]" in line:
                theta4.append(line.split()[1])

    theta1 = np.array([float(f) for f in theta1])
    theta2 = np.array([float(f) for f in theta2])
    theta3 = np.array([float(f) for f in theta3])
    theta0 = np.array([float(f) for f in theta0])
    theta4 = np.array([float(f) for f in theta4])
    return theta0, theta1, theta2, theta3, theta4


def check_mcmc(freq):

    pslist, fitparams_all, mcmc_all, plind_fit, plind_mcmc, plind_mode = [], [], [], [], [], []

    popt = [2,9,0.67]
    pl = mle.pl(freq, *popt)
    for n in range(1000):
        ps = powerspectrum.PowerSpectrum()
        ps.freq = freq                               
        ps.ps = np.array(np.random.chisquare(2, size=len(freq))*pl)
        pslist.append(ps)


    for i,p in enumerate(pslist):
        fitspec = mle.PerMaxLike(p, fitmethod="constbfgs", obs=True)
        fitparams = fitspec.mlest(mle.pl, [2,3,0.67])
        fitparams_all.append(fitparams)
        plind_fit.append(fitparams["popt"][0])
        mcobs = bayes.MarkovChainMonteCarlo(p, topt=fitparams["popt"], tcov=fitparams["cov"], niter=100, nchain=500, func=mle.pl, check_conv=True, use_emcee=True, plot=False, namestr='mcmc_test'+str(i))
        mcall = mcobs.mcall
        plind_mcmc.append(mean(mcall[0]))
        histo, bin_edges = np.histogram(mcall[0], bins=150, range=(0.0, 8.0))
        plindex = np.where(histo == max(histo))
        plind_mode.append(bin_edges[plindex][0])

   
        pldict = {}
        pldict["fit"] = plind_fit
        pldict["mcmc_mean"] = plind_mcmc
        pldict["mcmc_mode"] = plind_mode
        pldict["mcmc"] = mcall
        pldict["psfit"] = fitparams_all

    return pldict


def bursts_after_analysis(datadir="./"):

    alldict = {}

    filename = glob.glob(datadir+"*burstfile*")
    fsplit = [f.split("_")[:2] for f in filename]
    keys = [f[0].split("/")[-1] for f in fsplit]


    for k in keys:
        alldict[k] = []

    for f,k in zip(filename, keys):
        burst = gt.getpickle(f)
        alldict[k].append(burst)

    return alldict



def make_stack(alldictnew):
    
    blen = []
    for key, lis in alldictnew.iteritems():
        for l in lis:
            blen.append(l.blen)

    tmax = max(blen)
    psno = 1.4*tmax*4096.0  
    print("tmax: " + str(tmax))
    print("ps no: " + str(psno))
    psstack = np.zeros(psno)
    pslist = []
    lclist = []

    for key, lis in alldictnew.iteritems():
        procdata = gt.getpickle('tte_bn' + str(key) + '_procdata.dat')
        evt = procdata["combined"]
        time = np.array([x.time for x in evt.photons])
        for l in lis:
            minend = time.searchsorted(l.bst - 0.2*tmax)
            maxend = time.searchsorted(l.bst + 1.2*tmax)
            tnew = time[minend:maxend]
            lc = lightcurve.Lightcurve(tnew, timestep=0.5/4096.0, tseg=1.4*tmax)
            ps = powerspectrum.PowerSpectrum(lc, norm='leahy')
           
            psstack = psstack + ps.ps
            pslist.append(ps)
            lclist.append(lc)
    psstack = psstack/float(len(pslist))
    return psstack, pslist, lclist

def compute_ratios(alldict, pslist):
    ratios = []
    counter = 0
    for key, lis in alldict.iteritems():
        for i,l in enumerate(lis):
            try:
                b = l.psfit
                popt = b["popt"]
                if b["model"] == "pl":
                    model = mle.pl
                elif b["model"] == "bpl":
                    model = mle.bpl
                psfit = model(pslist["counter"].freq, *popt)
                psratio = pslist[counter].ps/psfit
                ratios.append(psratio)
                counter = counter + 1
            except NameError, AttributeError:
                counter = counter + 1
                continue

    return ratios




